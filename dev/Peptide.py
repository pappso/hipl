import sys

from Formula import Formula
from Residue import Residue

class Peptide:

    def __init__(self, name, formula_z_1, charge = 1):
        '''Initialize the residue list with a M+H z=1 formula string
        name is the name of the peptide, like pepa1a1 4
        formula_z_1 is the formula with 1 proton C102H152O32N21S0
        charge is the charge of the peptide (assumed to be 1 by default).'''

        self.name = name
        if not self.name:
            print("Error, the name argument needs to be set")
            exit(1)

        self.charge = charge

        self.formulaZ1 = formula_z_1;
        if not self.formulaZ1:
            print("Error, the formula for M+H argument needs to be set")
            exit(1)

    def __str__(self):
        peptide_string = self.name + " "
        peptide_string += str(self.charge)
        peptide_string += " "
        peptide_string += self.formulaZ1

        return peptide_string


    def get_charge(self):
        return self.charge

    def get_name(self):
        return self.name


    def get_protonated_peptide_formula(self):
        '''Get the formula of the peptide incremented by the right number of
        protons'''

        # The peptide contains its own formula that corresponds to H+ (that is
        # M+H). We thus need to take that formula and add the missing number of 
        # protons, that is (z-1).

        # print("self.formulaZ1: " + self.formulaZ1)

        working_formula = Formula(self.formulaZ1)

        # print("Working formula: " + working_formula.get_formula_string())
        
        # print("Self charge: " + str(self.charge))

        if(self.charge > 1):
            working_formula.increment_formula("H" + str(self.charge - 1));
            # print("z = " + str(self.charge))

        # print("Fully protonated formula: " + working_formula.get_formula_string())

        return working_formula.get_formula_string()

