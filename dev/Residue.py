from Formula import Formula
import re


class Residue:

    def __init__(self, name, code, formula_string):
        '''Initialize the residue with name, code and formulat strings'''
        self.name = name 
        self.code = code

        if not formula_string:
            print("A Residue instance cannot be created without initializing the formula")

            import sys
            exit(1)

        self.formula = Formula(formula_string)


    def __str__(self):
        residue_string = self.name + " " + self.code + " " + self.get_formula_string()
        return residue_string


    def reset_formula(self):
        self.formula.clear()


    def set_formula(self, formula_string, clear = True):
        '''Set the "formula_string" as the formula of this Residue'''
        self.formula.set_formula(formula_string, clear)


    def get_formula_string(self):
        '''Returns the formula of this Residue as a string'''

        return self.formula.get_formula_string()


    def increment_formula(self, formula_string):
        '''Add to this Residue's formula another formula'''

        self.formula.increment_formula(formula_string)

