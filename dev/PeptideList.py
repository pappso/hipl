import sys
import re
from Peptide import Peptide

class PeptideList:

    def __init__(self, file_name):

        self.file_name = file_name 
        self.peptide_list = []

    def set_file_name(self, file_name):
        self.file_name = file_name

    def clear(self):
        self.peptide_list.clear()

    def load_peptides(self, file_name):
        if not file_name:
            file_name = self.file_name
        else:
            self.file_name = file_name

        file = open(file_name, "r")

        modif_string = ""

        for line in file:
            
            line = line.strip()
            if not line:
                continue;

            # print("parsing peptide line: " + line)

            # pepa1a1 4       [C 102 H 152 O 32 N 21 S 0]

            # regexp = re.compile("([^\s.]+)\s+([1-4])\s+\[([\sCHONSP\d]+)\]")

            # We do not use the charge between the pep name and its formula
            regexp = re.compile("([^\s.]+)\s+\[([\sCHONSP\d]+)\]")

            match = regexp.match(line)

            if match != None:

                # print("match without modif: " + match.group(0))

                name = match.group(1)
                
                # No more charge field.
                # charge = match.group(2)

                formula_z_1 = match.group(2)
                # print("Not Stripped Formula: " + formula_z_1)

                # The formula needs to be stripped of its whitespace, whatever
                # it be (tab, space, newline)
                formula_z_1 = "".join(formula_z_1.split())
                # print("Stripped Formula: " + formula_z_1)

                peptide = Peptide(name = name, charge = 1, formula_z_1 = formula_z_1)
                self.peptide_list.append(peptide)
                # print("Appended peptide: " + str(peptide))

            else:
                print("Failed to parse line '" + line + "'")


        # At this point all the peptides have been loaded.

        print("Number of peptides: " + str(len(self.peptide_list)))
        # for peptide in self.peptide_list:
            # print("Peptide: " + str(peptide))


    def peptide_count(self):
        return len(self.peptide_list)


    def print_all_peptides(self):
        for peptide in self.peptide_list:
            print(str(peptide))








