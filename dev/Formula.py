import re

class Formula:

    def __init__(self, formula_string):
        '''Constructs a Formula starting from a string like C102H152O32N21S0.'''

        self.formula = {}

        if not formula_string:
            print('''Cannot create a Formula instance without initializing it with
            a formula string''')
            import sys
            exit(1)

        # The formula is coded internally as a dictionary of { symbol: count }
        self.set_formula(formula_string, clear = True)


    def set_formula(self, formula, clear = True):
        ''' Constructs the chemical composition dictionary by parsing the formula argument'''

        # First clear the formula if so is required.
        if clear == True:
            self.formula.clear()
            # print("after clear type of self.formula: " + str(type(self.formula)))

        # Now parse the formula string and extract all the symbol/count pairs.
        regexp1 = re.compile('[A-Z][a-z]?[0-9]+')

        # Find all the occurrences of C1 or Ca3 or C12 or Ca33
        match_list = regexp1.findall(formula)

        if match_list != None:
            # print(match_list)

            # At this point iterate in the list and start feeding the dictionary

            for pair in match_list:
                #print(pair)

                regexp2 = re.compile('([A-Z][a-z]?)([0-9]*)')
                match = regexp2.match(pair)

                if match != None:
                    # print(match.groups())
                    atom_symbol = match.group(1)
                    # print("atom_symbol: " + atom_symbol)

                    atom_count = match.group(2)
                    # print("atom_count: " + atom_count)

                    # Note that if the formula says C2H4NO,
                    # atom_count for N and O are 0. But it
                    # needs to be incremented to one.

                    if not atom_count:
                        ++atom_count

                    if self.formula.get(atom_symbol) != None:
                        self.formula[atom_symbol] += int(atom_count)
                    else:
                        self.formula[atom_symbol] = int(atom_count)

            # At this point we have finished parsing the formula
            # print("Done parsing the formula")
            # for symbol in self.formula:
                # print("symbol: " + symbol + " count: " + str(self.formula.get(symbol)))
        else:
            print("No match !")

    def clear(self):
        self.formula.clear()


    def get_formula_string(self):
        '''Returns this Formula as a string'''

        # print("type of self.formula: " + str(type(self.formula)))

        formula_string = ""

        for symbol in self.formula:
            # print("symbol: " + symbol)
            # print(" count: " + str(self.formula.get(symbol)))
            formula_string += symbol
            formula_string += str(self.formula.get(symbol))
        
        return formula_string


    def increment_formula(self, formula_string):
        '''Add to this Formula another formula'''
        self.set_formula(formula_string, clear = False)
        






