import argparse

def set_isotope_probabilities(command_line_args, chemical_element):

    if chemical_element == "carbon":
        c_abundances = command_line_args.carbon
        carbon_probs = ()
        if not c_abundances:
            # Set the default ones
            carbon_probs = (0.9892119418505, 0.0107880581495331)
            print("Default carbon probabilities: " + str(carbon_probs))
        else:
            # Set the ones defined in the command line
            carbon_probs = c_abundances
            print("Command line-set carbon probabilities: " + str(carbon_probs))

        return carbon_probs

    if chemical_element == "hydrogen":
        abundances = command_line_args.hydrogen
        hydrogen_probs = ()
        if not abundances:
            # Set the default ones
            hydrogen_probs = (0.9998842901, 0.00011570983)
            print("Default hydrogen probabilities: " + str(hydrogen_probs))
        else:
            # Set the ones defined in the command line
            hydrogen_probs = abundances
            print("Command line-set hydrogen probabilities: " + str(hydrogen_probs))

        return hydrogen_probs

    if chemical_element == "oxygen":
        abundances = command_line_args.oxygen
        oxygen_probs = ()
        if not abundances:
            # Set the default ones
            oxygen_probs = (0.99756760972956, 0.000380998476, 0.0020513917944)
            print("Default oxygen probabilities: " + str(oxygen_probs))
        else:
            # Set the ones defined in the command line
            oxygen_probs = abundances
            print("Command line-set oxygen probabilities: " + str(oxygen_probs))

        return oxygen_probs

    if chemical_element == "sulfur":
        abundances = command_line_args.sulfur
        sulfur_probs = ()
        if not abundances:
            # Set the default ones
            sulfur_probs = (0.94985001199904, 0.007519398448124, 0.042520598352132)
            print("Default sulfur probabilities: " + str(sulfur_probs))
        else:
            # Set the ones defined in the command line
            sulfur_probs = abundances
            print("Command line-set sulfur probabilities: " + str(sulfur_probs))

        return sulfur_probs

