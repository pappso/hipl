#! /usr/bin/env python3


# Packages to install in Debian:
# apt install python3-isospec python3-cffi
import os
import argparse
import IsoSpecPy
from math import exp
from Formula import Formula
import Utilities


try:
    if IsoSpecPy.__version__[:4] != '2.1.':
        raise AttributeError
except AttributeError:
    print("This file is meant to be used with IsoSpecPy version 2.0.X. You seem to have a different version installed on your system.")
    import sys
    sys.exit(-1)


parser = argparse.ArgumentParser()

parser.add_argument("peptide_list_file", help="absolute|relative path to the peptide list file")

parser.add_argument("-m", "--maxprob", type=float, help="fraction of the total probability to cover (default 0.99)")

parser.add_argument("-p", "--plot", help="plot each new isotopic cluster", action="store_true")

parser.add_argument("-o", "--sort", type=str, help="sort the (mz,prob) pairs by mz or prob (default mz)")

parser.add_argument("-d", "--delta", type=float, help="m/z delta beyond which a calculated centroid will be part of the next isotopic cluster peak [convolution resolution (default 0.01)]")

parser.add_argument("-b", "--begin", type=float, help="First 15N incorporation probability (default 0.00364198543205)")
parser.add_argument("-e", "--end", type=float, help="Last 15N incorporation probability (default 0.1)") 

parser.add_argument("-s", "--steps", type=int, help="Number of steps between [begin--end] N15 incorporation probabilities (default 10)")

# The user can set the abundances for the isotopes of chemical elements C and N.
parser.add_argument("-C", "--carbon", type=str, help="Provide the abundances for all the isotopes in the form \"(xxx.xxx,yyy.yyy)\"; if not set, default natural abundances are selected")
parser.add_argument("-H", "--hydrogen", type=str, help="Provide the abundances for all the isotopes in the form \"(xxx.xxx,yyy.yyy)\"; if not set, default natural abundances are selected")
parser.add_argument("-O", "--oxygen", type=str, help="Provide the abundances for all the isotopes in the form \"(xxx.xxx,yyy.yyy)\"; if not set, default natural abundances are selected")
parser.add_argument("-S", "--sulfur", type=str, help="Provide the abundances for all the isotopes in the form \"(xxx.xxx,yyy.yyy)\"; if not set, default natural abundances are selected")

args =  parser.parse_args()

peptide_list_file_path = args.peptide_list_file

if not args.maxprob:
    max_probability = 0.99
else:
    max_probability = args.maxprob

if args.plot:
    plot_clusters = True
else:
    plot_clusters = False

sorting = "nosort"
if args.sort == "mz":
    sorting = "mz"
if args.sort == "prob":
    sorting = "prob"

# By default, sort by mz.
if sorting == "nosort":
    sorting = "mz"

delta = 0.0
if not args.delta:
    delta = 0.01
else:
    delta = args.delta

begin = 0.003641985432058
if not args.begin:
    begin = 0.003641985432058
else:
    begin = args.begin

end = 0.1
if not args.end:
    end = 0.1
else:
    end = args.end

steps = 10
if not args.steps:
    steps = 10
else:
    steps = args.steps

# Now ensure we characterize all the chemical elements correctly, isotope-wise !


carbon_masses = (12.000000000000, 13.003354835200)
#carbon_probs = (0.9893, 0.0107)
carbon_probs = Utilities.set_isotope_probabilities(args, "carbon")
print("Carbon probabilities: " + str(carbon_probs))

hydrogen_masses = (1.007825032270, 2.014101778190)
#hydrogen_probs = (0.999885, 0.000115)
hydrogen_probs = Utilities.set_isotope_probabilities(args, "hydrogen")
print("Hydrogen probabilities: " + str(hydrogen_probs))

oxygen_masses = (15.994914620200, 16.999131757600, 17.999159613700)
#oxygen_probs = (0.99757, 0.00038, 0.00205)
oxygen_probs = Utilities.set_isotope_probabilities(args, "oxygen")
print("Oxygen probabilities: " + str(oxygen_probs))

sulfur_masses = (31.972071174100, 32.971458910100, 33.967867030000)
#sulfur_probs = (0.9493, 0.0076, 0.0429, 0.0002)
sulfur_probs = Utilities.set_isotope_probabilities(args, "sulfur")
print("Sulfur probabilities: " + str(sulfur_probs))
 
nitrogen_masses = (14.003074004200, 15.000108899400) 

# At this point, load all the required stuff.

# The experimental peptide list
from PeptideList import PeptideList

peptide_list = PeptideList(peptide_list_file_path)
peptide_list.load_peptides("")

print("Loaded " + str(peptide_list.peptide_count()) + " peptides")

# We now need to construct a list of Nitrogen15 percentages to be used for the
# calculations.
# Imagine [begin-end] and steps are [0.35-8.5] 5
# steps is the number of slots, comprising the 2 [begin-end] slots.

step = (end - begin) / steps

print("calculated step:" + str(step))

n15_probs_list = []

iteration = 0

while(iteration <= steps):
    new_percentage = begin + (iteration * step)
    n15_probs_list.append(new_percentage)
    print("appended new prob value:" + str(new_percentage))
    iteration += 1

# At this point, we have to iterate in that list and for each prob compute the
# isotopic cluster of the various peptides in the list.

for n15_prob in n15_probs_list:
    filename =  peptide_list_file_path + "_prob_" + str(n15_prob);
    print("filename: " + filename)

    file_handle = open(filename, 'w')

    # Now that we have the file handle open, we can start iterating in the
    # various peptides of the list and perform

    # 1. the configuration of the itosopes
    # 2. the computation itself
    # 3. the writing to the file of the centroids of the cluster.

    # The normal percentages for nitrogen
    #n14_prob = 0.99635801456794171
    #n15_prob = 0.003641985432058

    # But in this software we want to provide the user the capability to set a
    # number of different 15N probabilities within a given range and with a
    # given step.
    n14_prob = 0.9999999999999 - n15_prob
    print("n14_prob: " + str(n14_prob))
    print("n15_prob: " + str(n15_prob))
    print("n14_prob + n15_prob: " + str(n14_prob + n15_prob))
    nitrogen_probs = (n14_prob, n15_prob)


    for peptide in peptide_list.peptide_list:
    
        # print("peptide mono charged formula: " + peptide.formulaZ1)
        # print("peptide z charge: " + str(peptide.charge))
        peptide_formula = peptide.get_protonated_peptide_formula()
        # print("peptide z protonated formula: " + peptide_formula)
    
        # The peptide has this formula from the MassChroQ output [C 174 H 261 O 55 N 42 S 2]
        # That is then processed into a dictionary, like { C : 174 , H : 261 }
        
        print("Calculating isotopic distribution of peptide (p 15N: " 
            + str(n15_prob) 
            + ") "
            + str(peptide) 
            + " with formula: " 
            + peptide_formula)

 
        # file_handle.write("Calculating isotopic distribution of peptide (p 15N: " 
            # + str(n15_prob) 
            # + ") "
            # + str(peptide) 
            # + " with formula: " 
            # + peptide_formula
            # + "\n")

        file_handle.write(str(peptide.get_name()) + " ");

        # Create the dictionary form for the formula
        dict_formula = Formula(peptide_formula)

        atom_count_list = [ dict_formula.formula["C"], dict_formula.formula["H"],
        dict_formula.formula["O"], dict_formula.formula["N"], dict_formula.formula["S"] ] ;
    
        print("atom_count_list:" + str(atom_count_list))

        # First natural isotope abudance version
        # i = IsoSpecPy.IsoTotalProb(formula=peptide_formula, prob_to_cover = max_probability, get_confs=False)

        # Beware that we need not provide the formula if we provide the
        # atomCount in the form of atom_count_list (otherwise, the formula is
        # multiplied by 2 !!!
        i = IsoSpecPy.Iso(formula = "", get_confs = False, atomCounts = atom_count_list,
            isotopeMasses = [carbon_masses, hydrogen_masses, oxygen_masses, nitrogen_masses, sulfur_masses], 
            isotopeProbabilities = [carbon_probs, hydrogen_probs, oxygen_probs, nitrogen_probs, sulfur_probs],
            use_nominal_masses = False, fasta = "");

        from IsoSpecPy import isoFFI
        tabulator = isoFFI.clib.setupTotalProbFixedEnvelope(i.iso,
            max_probability, False, False)
        ido = IsoSpecPy.IsoDistribution(cobject = tabulator, get_confs = False, iso = i)
        isoFFI.clib.deleteFixedEnvelope(tabulator, False)
    
        if sorting == "prob":
            ido.sort_by_prob()
        elif sorting == "mz":
            ido.sort_by_mass()
        
        # print("Number of configurations: " + str(len(i)))
    
        # print("Type of i: " + str(type(i)))
    
        # Create a list of (mz,i) tuples with
        # the unadulterated results
        result_list = []
    
        # Create a list of (mz,i) tuples with the final convoluted results
        # because the output of the isospec computation provides a resolution
        # that is too high with respect of what we need, so we'll have to
        # convolute the returned results into bins the size of which was
        # arbitrarily set to 0.01.

        final_result_list = []
        
        for mass, prob in ido:
            result_list.append((mass,prob))
            # Print to the console the isotopic cluster centroids and their probability
            # output_string = str(mass)
            # output_string += ","
            # output_string += str(prob)
            # print(output_string)
    
        # We now have the list of isotopic cluster centroids. But
        # we need to somehow convolute them, because there might be
        # centroids that are so close that they would be part of 
        # the same mass peak. So, we iterate in the tuple list and we check
        # if the next tuple has m/z value so near to the previous one that it
        # needs to be accounted for as if it were the previous one. We just keep
        # filling in a list of tuples that are so close that they belong to the
        # same peak. We do an average of all the m/z values and a sum of all the
        # probs, so that we have convolute multiple centroids so close together
        # into one single averated m/z value centroid and summed-up probs.
    
        # By essence, the first centroid will be the lightest mz (we should 
        # sort the results by mz. So This first centroid is to be copied as is 
        # in the new final_result_list.
        is_first_tuple = True
        is_averaging = False
        last_tuple = (0.0,0.0)
        averaging_tuple_list = []
     
        for iter_tuple in result_list:
    
            # print("Newly iterated tuple: " + str(iter_tuple))
    
            if is_first_tuple == True:
                # print("That is the first tuple")
                averaging_tuple_list.append(iter_tuple);
                is_first_tuple = False;
                continue;
    
            # Now we need to check if the next centroid can be fused together with
            # the precendent or if it is genuinely another centroid.
    
            # The first value [0] of the tuple is the m/z value and the second
            # value [1] is the isotopic prob.

            if iter_tuple[0] >= (averaging_tuple_list[-1][0] + 0.95):
    
                # The new tuple is genuinely a new centroid
                # We need to average the mz values of the 
                # centroids being held in the averaging_tuple_list.
    
                # print("Iterated tuple genuinely a new centroid peak")
    
                mz_sum = 0.0
                mz_count = 0.0
                i_sum = 0.0
    
                # print("Averaging the tuples in the averaging list")
    
                for averaging_tuple in averaging_tuple_list:
    
                    # print("Iterating in averaging_tuple: " + str(averaging_tuple))
                    # print("mz: " + str(averaging_tuple[0]))
                    # print("i: " + str(averaging_tuple[1]))
    
                    mz_sum += averaging_tuple[0]
                    mz_count += 1
                    i_sum += averaging_tuple[1]
    
                    # print("while iterating: mz_sum: " + str(mz_sum) + " mz_count: " + str(mz_count) + " i_sum: " + str(i_sum))
    
                # A this point we can create the new tuple
    
                # print("after iteration: mz_sum: " + str(mz_sum) + " mz_count: " + str(mz_count) + " i_sum: " + str(i_sum))
                new_averaged_tuple = (mz_sum / mz_count, i_sum)
    
                # print("Newly averaged tuple: " + str(new_averaged_tuple))
    
                final_result_list.append(new_averaged_tuple)
    
                # print("Appended that new_averaged_tuple to the final result list")
                # print("Final result list has size: " + str(len(final_result_list)))
    
                # Finally clear the averaging_tuple_list and store the new_averaged_tuple in it
    
                averaging_tuple_list.clear()
    
                # print("Cleared the averaging_tuple_list that has now size: " + str(len(averaging_tuple_list)))
    
                averaging_tuple_list.append(iter_tuple)
    
                # print("Appended the iter_tuple to the averaging_tuple_list that has now size: " + str(len(averaging_tuple_list)))
    
                # Now we have done the job, go to the next tuple in result_list.
    
            else:
                # print("Iterated tuple is not a genuinely new centroid peak")
                averaging_tuple_list.append(iter_tuple)
                # print("Appended it to the averaging_tuple_list that has now size: " + str(len(averaging_tuple_list)))
    
        # At this point we are out of the loop because the last centroid peak in
        # the IsoSpec-generated configurations was parsed and is still in
        # iter_tuple. 
    
        # If that last centroid peak had been a genuinely new isotopic peak,
        # then, it would have triggered the calculation of the
        # new_averaged_tuple and itself would have been stored in
        # averaging_tuple_list.
    
        # If that last centroids peak had been NOT a genuinely new isotopic
        # peaks, then it would have been appended to the averaging_tuple_list.
    
        # We see that in the first case, the averaging_tuple_list must be of
        # size 1, while in the second case, that list must be of size > 1.
    
        if len(averaging_tuple_list) == 1:
            # print("Last iter_tuple was a genuinely new isotopic peak")
            final_result_list.append(iter_tuple)
        elif len(averaging_tuple_list) > 1:
            # print("Last iter_tuple was NOT a genuinely new isotopic peak")
            # print("Need to compute the new_averaged_tuple and append it to final_result_list")
    
            mz_sum = 0.0
            mz_count = 0.0
            i_sum = 0.0
    
            for averaging_tuple in averaging_tuple_list:
    
                # print("Iterating in averaging_tuple: " + str(averaging_tuple))
                # print("mz: " + str(averaging_tuple[0]))
                # print("i: " + str(averaging_tuple[1]))
    
                mz_sum += averaging_tuple[0]
                mz_count += 1
                i_sum += averaging_tuple[1]
    
                # print("while iterating: mz_sum: " + str(mz_sum) + " mz_count: " + str(mz_count) + " i_sum: " + str(i_sum))
    
            # A this point we can create the new tuple
    
            # print("after iteration: mz_sum: " + str(mz_sum) + " mz_count: " + str(mz_count) + " i_sum: " + str(i_sum))
            new_averaged_tuple = (mz_sum / mz_count, i_sum)
    
            # print("Newly averaged tuple: " + str(new_averaged_tuple))
    
            final_result_list.append(new_averaged_tuple)
    
            # print("Appended that new_averaged_tuple to the final result list")
            # print("Final result list has size: " + str(len(final_result_list)))
        else:
            print("Cannot be that averaging_tuple_list is 0-size")
            exit(1)
    
        # At this point we should be able to print the final_result_list:
        # print("\nConvoluted itotopic cluster:\n")
    
        # Store the first m/z value's probability that we'll need to compute the
        # ratio for each member of the isotopic cluster.

        first_mz_prob = final_result_list[0][1]
        # print("The first prob of the cluster is:" + str(first_mz_prob) + "\n")
        # file_handle.write("The first prob of the cluster is:" + str(first_mz_prob) + "\n")

        for iter_tuple in final_result_list:
            # Print to the console.
            print(str(iter_tuple[1] / first_mz_prob) + " ")
            # Print to the right file.
            # This used to be the first stanza-based output of the peptide's
            # cluster: m/z value and prob, then newline.
            # file_handle.write(str(iter_tuple[0]) + "," + str(iter_tuple[1]) + "\n")
            # This only output the probs of the cluster on a single line.
            # file_handle.write(str(iter_tuple[1]) + " ")
            # This is the most recent Willy's requirement: to output the ratio
            # of each centroid's prob value against the *first* prob value for
            # the first isotopic cluster.
            file_handle.write(str(iter_tuple[1] / first_mz_prob) + " ")

    
        # Print to the console.
        print("\n")
        # Print to the right file.
        file_handle.write("\n")
    
        if plot_clusters:
            ido.plot()

    # Finally close the file
    file_handle.close()

import sys

exit(0)



i = IsoSpecPy.IsoTotalProb(formula="H2O1", prob_to_cover = 0.999, get_confs=True)

print("Calculating isotopic distribution of water. Here's a list of configurations necessary to cover at least 0.999 of total probability:")

for mass, prob, conf in i:
    print("")
    print("Mass: " + str(mass))
    print("probability: " + str(prob))
    print("Number of Protium atoms: " + str(conf[0][0]))
    print("Number of Deuterium atoms: " + str(conf[0][1]))
    print("Number of O16 atoms: " + str(conf[1][0]))
    print("Number of O17 atoms: " + str(conf[1][1]))
    print("Number of O18 atoms: " + str(conf[1][2]))

print("")
print("Now what if both isotopes of hydrogen were equally probable, while prob. of O16 was 50%, O17 at 30% and O18 at 20%?")

hydrogen_probs = (0.5, 0.5)
oxygen_probs = (0.5, 0.3, 0.2)
hydrogen_masses = (1.00782503207, 2.0141017778)
oxygen_masses = (15.99491461956, 16.99913170, 17.9991610)

i = IsoSpecPy.IsoTotalProb(atomCounts = (2, 1), isotopeMasses = (hydrogen_masses, oxygen_masses), isotopeProbabilities = (hydrogen_probs, oxygen_probs), prob_to_cover = 0.999, get_confs=True)



print("The first configuration has the following parameters:")
print("Mass: " + str(i.masses[0]))
print("probability: " + str(i.probs[0]))
conf = i.confs[0]
conf_hydrogen = conf[0]
conf_oxygen = conf[1]
print("Number of Protium atoms: " + str(conf_hydrogen[0]))
print("Number of Deuterium atoms: " + str(conf_hydrogen[1]))
print("Number of O16 atoms: " + str(conf_oxygen[0]))
print("Number of O17 atoms: " + str(conf_oxygen[1]))
print("Number of O18 atoms: " + str(conf_oxygen[2]))






