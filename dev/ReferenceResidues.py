import re
from Residue import Residue

class ReferenceResidues:

    def __init__(self, file_name):
        self.file_name = file_name 
        self.residue_list = []

    def set_file_name(self, file_name):
        self.file_name = file_name

    def clear(self):
        self.residue_list.clear()

    def load_residues(self, file_name):
        if not file_name:
            file_name = self.file_name
        else:
            self.file_name = file_name

        file = open(file_name, "r")

        for line in file:
            
            # print("line: " + line)

            # Glycine G C2H3N1O1
            regexp = re.compile("([A-Z][a-z]+)\s+([A-Z])\s+([A-Za-z\d]+)")
            match = regexp.match(line)

            if match != None:

                # print("match: " + match.group(0))

                name = match.group(1)
                code = match.group(2)
                formula_string = match.group(3)

                residue = Residue(name, code, formula_string)

                # print("residue: " + str(residue))
                # print("name: " + residue.name)
                # print("code: " + residue.code)
                # print("formula: " + residue.get_formula_string())

                self.residue_list.append(residue)
            else:
                print("Line " + line + " did not match regexp !\n")

        # At this point all the residues have been loaded.
        # print("Number of residues: " + str(len(self.residue_list)))


    def residue_count(self):
        return len(self.residue_list)


    def print_all_residues(self):
        for residue in self.residue_list:
            print(str(residue))


    def get_residue_by_code(self, code):
        '''Return the Residue object that has code "code"'''
        match_list = [residue for residue in self.residue_list if residue.code == code] 

        if len(match_list) > 1:
            print("Error, there are multiple copies of the same residue: " +
                    residue.name)

        return match_list[0]


    def get_residue_formula_by_code(self, code):
        '''Return the formula of the residue that has code "code"'''
        match_list = [residue for residue in self.residue_list if residue.code == code] 
        if len(match_list) > 1:
            print("Error, there are multiple copies of the same residue: " +
                    residue.name)

        print(str(match_list[0].get_formula_string()));










